﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointexPlus : MonoBehaviour {
    public float MaxPoint;
    public float PlusPoint;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Light>().range += PlusPoint;
       
        if (GetComponent<Light>().range >= MaxPoint)
        {
            Destroy(gameObject);
        }
	}
}
