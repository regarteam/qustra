﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalInfo: MonoBehaviour {
	public  GameObject SkillBox { get; set; }
	public  bool Skill { get; set; }
	public  int NumHost { get; set; }
	public  List<Host> Hosts = new List<Host>();
	public  float MaxHp;
	public  float MaxMp;
	public PlayerStat PlStat;
	void Update() {
		Hosts = GlobalStaticInfo.Hosts;
		SkillBox = GlobalStaticInfo.SkillBox;
		Skill = GlobalStaticInfo.Skill;
		NumHost = GlobalStaticInfo.NumHost;
		MaxHp = GlobalStaticInfo.MaxHp;
		MaxMp = GlobalStaticInfo.MaxMp;
		PlStat = GlobalStaticInfo.PlStat;
	}
}
