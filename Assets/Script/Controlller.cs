﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Controlller : MonoBehaviour {
	public float CheckDistance = 2;
	public float CheckGraunded = 0.2f;
	public float SendRate = 0.02f;
	public GameObject CamTarget;
	public Transform mCam;
	public List<Item> items = new List<Item>();
	public GameObject firePoint;

	private float speedMove = 1000;
	private float speedRot = 15;
	private float heigthJump = 100;
	private Vector3 mMove;

	private Transform _mCam;
	private Vector3 mCamForward;

	private Rigidbody rigid;
	private Player localPlayer { get; set; }
	private Character charact;
	private Animator _animator;

	private float v;
	private float h;
	private float j;
	private bool graunded = true;
	private bool control;
	private bool jump;

	private float positionY;
	private float positionDeltaY;

	private float timeFire = 0;
	private Vector3 origin = new Vector3();
	private Vector3 target = new Vector3();
	
	public void OnEnabled(Transform cam, Player player) {
		localPlayer = player;
		items = player.items;
		mCam = cam;
		_mCam = mCam.GetComponentInChildren<Camera>().transform;
		_animator = GetComponent<Animator>();
		rigid = GetComponent<Rigidbody>();
		charact = GetComponent<Character>();
		speedMove = GlobalStaticInfo.PlStat.SpeedFly;
	}

	void Update() {
		charact.LerpAnimation(h, v, graunded, positionY, false);

		if (!localPlayer.Control) return;
		CheckGroundStatus();
		FireControll();
	}

	void FixedUpdate() {
		if (localPlayer == null) {
			var p = PhotonServer.Instance.Players.FirstOrDefault(
				n => n.CharacterName.Equals(PhotonServer.Instance.CharacterName));
			if (p != null) {
				localPlayer = p;
			}
			return;
		}

		if (!localPlayer.Control)
			return;
		else 
			Controll();
	}

	void Controll() {
		h = Input.GetAxis("Horizontal");
		v = Input.GetAxis("Vertical");
		j = Input.GetAxis("Fly");
		if (graunded) {
			if (mCam != null) {
				mCamForward = Vector3.Scale(_mCam.forward, new Vector3(1, 0, 1)).normalized;
				mMove = v * mCamForward + h * _mCam.right;
			}
			if (Input.GetKeyDown(KeyCode.Space)) {
				jump = true;
			}
		}
		else {
			rigid.useGravity = false;
			if (mCam != null) {
				mCamForward = Vector3.Scale(_mCam.forward, new Vector3(1, 1, 1)).normalized;
				mMove = v * mCamForward + h * _mCam.right;
			}
			mMove += new Vector3(0, j, 0);
		}
		mMove.Normalize();
		RaycastHit hitInfo;
		if (Physics.Raycast(transform.position + (transform.up * 1.1f), mMove, out hitInfo, CheckDistance)) {
			Vector3 norm = hitInfo.normal.normalized;
			if (norm.x == -mMove.x && norm.y == -mMove.y && norm.z == -mMove.z) {
				mMove = Vector3.zero;
			}
			else {
				Vector3 ax = Vector3.Cross(norm, mMove).normalized;
				Quaternion angl = Quaternion.AngleAxis(90, ax);
				Vector3 move = angl * norm;
				mMove = Vector3.Project(mMove, move);
				mMove = mMove / 2;
			}
		}
		Rot(graunded, mMove);
		if (jump) Jump();
		TrySend(transform.position + mMove * speedMove);
	}

	void FireControll() {
		timeFire += Time.deltaTime;
		int layerMask = 1 << 9;
		layerMask = ~layerMask;
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width * 0.5f, (Screen.height * 0.6f), 0));
		if (Input.GetMouseButtonDown(0) && timeFire > items[0].speedAttack && localPlayer.ManaPoint >= items[0].price) {
			timeFire = 0;
			localPlayer.ManaPoint -= items[0].price;
			if (Physics.Raycast(ray, out hit)) {
				origin = firePoint.transform.position;
				target = hit.point;
				if(target == null) {
					target = ray.direction * 1000f;
				}
				PhotonServer.Instance.FireOperation(target, origin, items[0].nameSlill);
			}
		}
	}

	private void Rot(bool graunded, Vector3 move) {
		Quaternion direct = new Quaternion();
		if (!graunded) {
			if (move.x == 0 && move.z == 0) {
				Quaternion a = new Quaternion(0, _mCam.rotation.y, 0, _mCam.rotation.w);
				direct = Quaternion.Lerp(transform.rotation, a, speedRot * Time.deltaTime);
			}
			else
				direct = Quaternion.Lerp(transform.rotation, _mCam.rotation, speedRot * Time.deltaTime);
		}
		else {
			Quaternion a = new Quaternion(0, _mCam.rotation.y, 0, _mCam.rotation.w);
			direct = Quaternion.Lerp(transform.rotation, a, speedRot * Time.deltaTime);
		}
		transform.rotation = direct;
	}

	void Jump() {
		rigid.AddForce(Vector3.up * heigthJump);
	}

	void CheckGroundStatus() {
		RaycastHit hitInfo;
#if UNITY_EDITOR
#endif
		if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, CheckGraunded)) {
			graunded = true;
			positionY = transform.position.y;
		}
		else {
			graunded = false;
		}
	}

	private void TrySend(Vector3 move) {
		PhotonServer.Instance.MoveOperation(move.x, move.y, move.z);
		PhotonServer.Instance.RotOperation(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
		PhotonServer.Instance.AnimOperation(h, v, positionY, graunded);

		if (jump) {
			PhotonServer.Instance.JumpOperation(jump);
			jump = false;
		}
	}
	
}