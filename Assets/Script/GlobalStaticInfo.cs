﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalStaticInfo{
	public static GameObject SkillBox { get; set; }
	public static bool Skill { get; set; }
	public static int NumHost { get; set; }
	public static List<Host> Hosts = new List<Host>();
	public static float MaxHp;
	public static float MaxMp;
	public static PlayerStat PlStat;
}
