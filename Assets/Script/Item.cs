﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item {
	public int type;
	public string nameSlill;
	public int level;
	public int price;
	public int numAnim;
	public GameObject prefab;
	public Sprite image;
	public int damage;
	public int speed;
	public float speedAttack;
	public TypeForce typeForce;
	public enum TypeForce {
		Fire,
		Ice,
		Electro,
		Necro,
	}
}
