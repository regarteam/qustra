﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour {
	
	private Animator _animator;
	private Rigidbody rigid;
	private float positionDeltaY;
	private float posY;
	private float h;
	private float v;
	private bool graund;
	public float lerp = 0.5f;
	public Player localPlayer;
	private Player ownerPlayer;
	public Transform cam { get; set; }
	public RectTransform text;

	void Awake() {
		_animator = GetComponent<Animator>();
		rigid = GetComponent<Rigidbody>();
		
	}
	private void Start() {
		ownerPlayer = GetComponent<Player>();
		text.GetComponent<Text>().text = ownerPlayer.CharacterName;
	}

	private void Update() {
		Anim();
		if (localPlayer != ownerPlayer) {
			if (cam == null) {
				cam = Camera.main.transform;
			}
			text.rotation = cam.rotation;
		}
	}

	void FixedUpdate() {
		if (localPlayer == null) {
			var p = PhotonServer.Instance.Players.FirstOrDefault(
				n => n.CharacterName.Equals(PhotonServer.Instance.CharacterName));
			if (p != null) {
				localPlayer = p;
				if(localPlayer == ownerPlayer) {
					text.gameObject.SetActive(false);
				}
			}
			return;
		}
	}

	private void Anim() {
		_animator.SetFloat("Turn", h);
		_animator.SetFloat("Forward", v);
		_animator.SetBool("Graunded", graund);
		if (!graund) {
			positionDeltaY = transform.position.y - posY;
			_animator.SetFloat("Jump", positionDeltaY);
		}
	}

	public void LerpAnimation(float _h, float _v, bool _graund, float _posY, bool _lerp) {
		if (_lerp) {
			h = Lerp(h, _h, lerp);
			v = Lerp(v, _v, lerp);
			posY = Lerp(posY, _posY, lerp);
			graund = _graund;
		}
		else {
			h = _h;
			v = _v;
			graund = _graund;
			posY = _posY;
		}
	}

	private float Lerp(float start, float end, float speed) {
		float cur;
		cur = start + ((end - start) * speed);
		return cur;
	}

	public void Damage(float damage) {
		PhotonServer.Instance.HitOperation(ownerPlayer.CharacterName, damage);
	}

	public void Move(Vector3 start, Vector3 end, float syncDelay, float syncTime = 0f) {
		syncTime += Time.deltaTime;
		transform.position = Vector3.Lerp(start, end, syncTime / syncDelay);
	}
}
