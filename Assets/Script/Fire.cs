﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Fire : MonoBehaviour {

	public Vector3 direct;
	public float speed;
	public float damage;
	public int type;
	public string ownerPlayer;
	private bool destro;

	private void Update() {
		if (destro) {
			Destroy(gameObject);
		}
		switch (type) {
			case 0:
				Move();
				break;
		}
	}

	private void Move() {
		transform.position += direct * speed * Time.deltaTime;
	}

	private void OnCollisionEnter(Collision collision) {
		switch (type) {
			case 0:
				BehaivorType0(collision); 
				break;
		}
	}

	private void BehaivorType0(Collision collision) {
		if (collision.gameObject.tag == "Player") {
			if (collision.gameObject.GetComponent<Player>().CharacterName == ownerPlayer) return;
		}
		if (collision.gameObject.tag == "Player") {
			collision.gameObject.GetComponent<Character>().Damage(damage);
		}
		destro = true;
	}
}
