﻿using UnityEditor;
using UnityEngine;

[System.Serializable]
public class PlayerStat{
	public string CharacterName;
	public float SpeedFly;
	public float SpeedRun;
	public float HP;
	public float MP;
	public float MagicForce;
	public float FireForce;
	public float IceForce;
	public float ElectroForce;
	public float NecroForce;
}
