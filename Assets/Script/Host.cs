﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

	[System.Serializable]
	public class Host{
		public int numHost;
		public string nameHost;
		public string sceneeHost;
		public int maxClients;
		public string Ip;
	}