﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class World : MonoBehaviour {
	private Player localPlayer { get; set; }

	private Vector3 oldPosition { get; set; }
	private float SendRate = 0.02f;
	private float lastSendTime = 0;
	private float suncTime;
	public float lerp = 0.5f;
	public int type = 0;
	public GameObject Gs;
	public GameObject itGs;
	private Dictionary<Player, GameObject> dic = new Dictionary<Player, GameObject>();
	private float y = 1;

	private bool visable = false;
	// Use this for initialization
	void Start() {
		PhotonServer.Instance.WorldEnterOperation(GlobalStaticInfo.NumHost);
		visable = false;
	}
	// Update is called once per frame
	void Update() {
		//MoveLogic();
		FireLogic();
		if (Input.GetKeyDown(KeyCode.Escape)) {
			visable = !visable;
			localPlayer.Control = !visable;
		}
		GsFunction();
	}

	void FixedUpdate() {
		if (localPlayer == null) {
			var p = PhotonServer.Instance.Players.FirstOrDefault(
				n => n.CharacterName.Equals(PhotonServer.Instance.CharacterName));
			if (p != null) {
				localPlayer = p;
				PhotonServer.Instance.ListPlayersOperation(GlobalStaticInfo.NumHost);
			}
			return;
		}
	}

	void MoveLogic() {
		suncTime += Time.deltaTime;
		if (suncTime >= SendRate) suncTime = 0;
		for (int i = 0; i < PhotonServer.Instance.Players.Count; i++) {
			var player = PhotonServer.Instance.Players[i];
			if (player != localPlayer) {
				player.transform.position = Vector3.Lerp(player.Position, player.newPosition, suncTime / player.deltaTime);
			}
		}
	}

	void FireLogic() {
		for(int i = 0; i < PhotonServer.Instance.Players.Count; i++) {
			var player = PhotonServer.Instance.Players[i];
			if (player.Fire) {
				Vector3 direct = player.Target - player.Origin;
				direct.Normalize();
				Item item = player.CurItem;
				Debug.Log(item.speed);
				var obj = Instantiate(item.prefab, player.Origin, Quaternion.LookRotation(direct));
				player.Fire = false;
				Fire fire = obj.AddComponent<Fire>();
				fire.direct = direct;
				fire.speed = item.speed;
				fire.ownerPlayer = player.CharacterName;
				fire.type = item.type;
				fire.damage = item.damage+player.neutralMagicForce+player.magicForce[item.typeForce];
			}
		}
	}

	private void OnGUI() {
		if (!visable) {
			return;
		}
		if(GUI.Button(new Rect(Screen.width / 2 - Screen.width / 20, Screen.height / 2 - Screen.height / 20, Screen.width / 10, Screen.height / 10), "В меню")) {
			PhotonServer.Instance.WorldExitOperation();
		}
	}

	private void GsFunction() {
		if (Input.GetKey(KeyCode.Tab)) {
			Gs.SetActive(true);
			if (dic.Count < PhotonServer.Instance.Players.Count) {
				for (int i = 0; i < PhotonServer.Instance.Players.Count; i++) {
					if (!dic.ContainsKey(PhotonServer.Instance.Players[i])) {
						Player pl = PhotonServer.Instance.Players[i];
						GameObject obj = Instantiate(itGs, Gs.transform);
						var rect = obj.GetComponent<RectTransform>();
						rect.anchorMax = new Vector2(1, y);
						rect.anchorMin = new Vector2(0, y - 0.1f);
						y -= 0.1f;
						var txts = obj.GetComponentsInChildren<Text>();
						foreach (var j in txts) {
							switch (j.gameObject.name) {
								case "Name":
									j.text = pl.CharacterName;
									break;
								case "Kill":
									j.text = pl.Kills.ToString();
									break;
								case "Dead":
									j.text = pl.Death.ToString();
									break;
							}
						}
						dic.Add(PhotonServer.Instance.Players[i], obj);
					}
				}
			}
			for (int i = 0; i < dic.Count; i++) {
				Player pl = dic.Keys.ToList()[i];
				var txts = dic[pl].GetComponentsInChildren<Text>();
				foreach (var j in txts) {
					switch (j.gameObject.name) {
						case "Kill":
							j.text = pl.Kills.ToString();
							break;
						case "Death":
							j.text = pl.Death.ToString();
							break;
					}
				}
			}
		}
		else Gs.SetActive(false);
	}
}
