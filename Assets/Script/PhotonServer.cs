﻿using ExitGames.Client.Photon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TestPhotonLib.Common;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotonServer : MonoBehaviour, IPhotonPeerListener {
	public string CONNECTION_STRING = "185.44.239.47:5055";
	private const string APP_NAME = "PhotonIntro";

	private static PhotonServer _instance;
	public static PhotonServer Instance {
		get { return _instance; }
	}

	public List<Player> Players = new List<Player>();

	public GameObject PlayerPrefab;
	public GameObject CameraPrefab;

	private PhotonPeer _peer { get; set; }

	public string CharacterName { get; set; }

	public event EventHandler<LoginEventArgs> OnLoginResponse;
	public event EventHandler<ChatMessageEventArgs> OnReceiveChatMessage;

	public List<Item> RegistryItems = new List<Item>();
	public Dictionary<int, Item> items = new Dictionary<int, Item>();
	public List<Item> l;

	private float deltaTime;
	private float syncTime;

	void Awake() {
		if (Instance != null) {
			DestroyObject(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
		_instance = this;
	}

	private void Start() {
		_peer = new PhotonPeer(this, ConnectionProtocol.Udp);
		Connect();
	}

	private void Update() {
		l = items.Values.ToList();
		if (_peer != null)
			_peer.Service();
		syncTime += Time.deltaTime;
	}

	void OnApplicationQuit() {
		WorldExitOperation();
		Disconnect();
	}

	private void Connect() {
		if (_peer != null)
			_peer.Connect(CONNECTION_STRING, APP_NAME);
	}

	private void Disconnect() {
		if (_peer != null) {

			_peer.Disconnect();
		}
	}

	public void DebugReturn(DebugLevel level, string message) {
	}

	public void OnOperationResponse(OperationResponse operationResponse) {
		switch (operationResponse.OperationCode) {
			case (byte)OperationCode.Login:
				LoginHandler(operationResponse);
				break;
			case (byte)OperationCode.ListPlayers:
				ListPlayersHandler(operationResponse);
				break;
			case (byte)OperationCode.ChoiceHost:
				ChoiceHostHandler(operationResponse);
				break;
			default:
				Debug.Log("Unknown OperationResponse:" + operationResponse.OperationCode);
				break;
		}
	}

	public void OnEvent(EventData eventData) {
		switch (eventData.Code) {
			case (byte)EventCode.Move:
				MoveHandler(eventData);
				break;
			case (byte)EventCode.Rot:
				RotHandler(eventData);
				break;
			case (byte)EventCode.Jump:
				JumpHandler(eventData);
				break;
			case (byte)EventCode.Anim:
				AnimHandler(eventData);
				break;
			case (byte)EventCode.Fire:
				FireHandler(eventData);
				break;
			case (byte)EventCode.Hit:
				HitHandler(eventData);
				break;
			case (byte)EventCode.WorldEnter:
				WorldEnterHandler(eventData);
				break;
			case (byte)EventCode.WorldExit:
				WorldExitHandler(eventData);
				break;
			case (byte)EventCode.CreateHost:
				CreateHostHandler(eventData);
				break;
			default:
				Debug.Log("Unknown Event:" + eventData.Code);
				break;
		}
	}

	public void OnStatusChanged(StatusCode statusCode) {
		switch (statusCode) {
			case StatusCode.Connect:
				Debug.Log("Connected to server!");
				break;
			case StatusCode.Disconnect:
				Debug.Log("Disconnected from server!");
				break;
			case StatusCode.TimeoutDisconnect:
				Debug.Log("TimeoutDisconnected from server!");
				break;
			case StatusCode.DisconnectByServer:
				Debug.Log("DisconnectedByServer from server!");
				break;
			case StatusCode.DisconnectByServerUserLimit:
				Debug.Log("DisconnectedByLimit from server!");
				break;
			case StatusCode.DisconnectByServerLogic:
				Debug.Log("DisconnectedByLogic from server!");
				break;
			case StatusCode.EncryptionEstablished:
				break;
			case StatusCode.EncryptionFailedToEstablish:
				break;
			default:
				Debug.Log("Unknown status:" + statusCode.ToString());
				break;
		}
	}



	#region SendOperations

	public void SendLoginOperations(string namePlayer) {
		_peer.OpCustom((byte)OperationCode.Login, new Dictionary<byte, object> { { (byte)ParameterCode.CharacterName, namePlayer } }, true);
	}

	public void MoveOperation(float x, float y, float z) {
		_peer.OpCustom((byte)OperationCode.Move,
							new Dictionary<byte, object>
								{
									{(byte) ParameterCode.PosX, x},
									{(byte) ParameterCode.PosY, y},
									{(byte) ParameterCode.PosZ, z},
								}, false);
	}

	public void RotOperation(float x, float y, float z, float w) {
		_peer.OpCustom((byte)OperationCode.Rot,
							new Dictionary<byte, object>
								{
									{(byte) ParameterCode.RotX, x},
									{(byte) ParameterCode.RotY, y},
									{(byte) ParameterCode.RotZ, z},
									{(byte) ParameterCode.RotW, w},
								}, false);
	}

	public void JumpOperation(bool jump) {
		_peer.OpCustom((byte)OperationCode.Jump, new Dictionary<byte, object> { { (byte)ParameterCode.Jump, jump }, }, true);
	}

	public void AnimOperation(float h, float v, float posY, bool graunded) {
		_peer.OpCustom((byte)OperationCode.Anim,
							new Dictionary<byte, object>
								{
									{(byte) ParameterCode.H, h},
									{(byte) ParameterCode.V, v},
									{(byte) ParameterCode.AnimPosY, posY},
									{(byte) ParameterCode.Graunded, graunded},
								}, false);
	}

	public void FireOperation(Vector3 target, Vector3 origin, string nameSkills) {
		float tx = target.x;
		float ty = target.y;
		float tz = target.z;
		float ox = origin.x;
		float oy = origin.y;
		float oz = origin.z;
		_peer.OpCustom((byte)OperationCode.Fire,
							new Dictionary<byte, object>
								{
									{(byte) ParameterCode.TargetX, tx},
									{(byte) ParameterCode.TargetY, ty},
									{(byte) ParameterCode.TargetZ, tz},
									{(byte) ParameterCode.OriginX, ox},
									{(byte) ParameterCode.OriginY, oy},
									{(byte) ParameterCode.OriginZ, oz},
									{(byte) ParameterCode.NameSkill, nameSkills},
								}, true);
	}

	public void HitOperation(string targetName, float damage) {
		_peer.OpCustom((byte)OperationCode.Hit, 
			new Dictionary<byte, object> {
				{ (byte)ParameterCode.TargetName, targetName },
				{ (byte)ParameterCode.Damage, damage }
			}, true);
	}

	public void WorldEnterOperation(int host) {
		_peer.OpCustom((byte)OperationCode.WorldEnter, new Dictionary<byte, object> { {(byte)ParameterCode.NumHost, host } }, true);
	}

	public void WorldExitOperation() {
		_peer.OpCustom((byte)OperationCode.WorldExit, new Dictionary<byte, object>(), true);
	}

	public void ListPlayersOperation(int host) {
		_peer.OpCustom((byte)OperationCode.ListPlayers, new Dictionary<byte, object> { { (byte)ParameterCode.NumHost, host } }, true);
	}

	public void ChoiceHostOperation(int host) {
		_peer.OpCustom((byte)OperationCode.ChoiceHost, new Dictionary<byte, object> { { (byte)ParameterCode.NumHost, host } }, true);
	}

	public void CreateHostOperation(int host, string nameHost, string sceeneHost) {
		object[] obj = { host, nameHost, sceeneHost };
		_peer.OpCustom((byte)OperationCode.CreateHost, new Dictionary<byte, object> {
			{ (byte)ParameterCode.HostInfo, obj },
			{ (byte)ParameterCode.NumHost, host } },
			true);
	}

	#endregion



	#region Handler

	void LoginHandler(OperationResponse response) {
		if (response.ReturnCode != 0) {
			ErrorCode errorCode = (ErrorCode)response.ReturnCode;
			switch (errorCode) {
				case ErrorCode.NameIsExist:
					if (OnLoginResponse != null)
						OnLoginResponse(this, new LoginEventArgs(ErrorCode.NameIsExist));
					Debug.Log("Error");
					break;
				default:
					Debug.Log("Error Login returnCode:" + response.ReturnCode);
					break;
			}

			return;
		}
		if (OnLoginResponse != null)
			OnLoginResponse(this, new LoginEventArgs(ErrorCode.Ok));
		Debug.Log("OK");

		var hosts = response.Parameters[(byte)ParameterCode.ListHostInfo] as Dictionary<int, object[]>;
		var stats = response.Parameters[(byte)ParameterCode.PlStat] as Dictionary<string, string>;

		Debug.Log("List Host Count: " + hosts.Count);
		foreach(var i in hosts.Values) {
			object[] hostInfo = i;
			Host host = new Host();
			host.numHost = (int)hostInfo[0];
			host.nameHost = (string)hostInfo[1];
			host.sceneeHost = (string)hostInfo[2];
			GlobalStaticInfo.Hosts.Add(host);
			Debug.Log("Add host with name: " + (string)i[1]);
		}

		PlayerStat pls = new PlayerStat();
		pls.CharacterName = stats["Name"];
		pls.HP = (float)Convert.ToDouble(stats["HP"]);
		pls.MP = (float)Convert.ToDouble(stats["MP"]);
		pls.SpeedFly = (float)Convert.ToDouble(stats["SpeedFly"]);
		pls.SpeedRun = (float)Convert.ToDouble(stats["SpeedRun"]);
		pls.MagicForce = (float)Convert.ToDouble(stats["MagicForce"]);
		pls.FireForce = (float)Convert.ToDouble(stats["FireForce"]);
		pls.IceForce = (float)Convert.ToDouble(stats["IceForce"]);
		pls.ElectroForce = (float)Convert.ToDouble(stats["ElectroForce"]);
		pls.NecroForce = (float)Convert.ToDouble(stats["NecroForce"]);
		GlobalStaticInfo.PlStat = pls;
	}

	private void MoveHandler(EventData eventData) {
		string characterName = (string)eventData.Parameters[(byte)ParameterCode.CharacterName];
		float posX = (float)eventData.Parameters[(byte)ParameterCode.PosX];
		float posY = (float)eventData.Parameters[(byte)ParameterCode.PosY];
		float posZ = (float)eventData.Parameters[(byte)ParameterCode.PosZ];

		var client = Players.FirstOrDefault(n => n.CharacterName.Equals(characterName));
		if (client == null) {
			Debug.Log("Move:not client");
			return;
		}
		if (client.newPosition != new Vector3 (posX, posY, posZ)) {
			client.newPosition = new Vector3(posX, posY, posZ);
			client.OldPosition = client.Position;
			client.deltaTime = syncTime;
			client.syncTime = 0;
		}
		syncTime = 0;
	}

	private void RotHandler(EventData eventData) {
		string characterName = (string)eventData.Parameters[(byte)ParameterCode.CharacterName];
		float rotX = (float)eventData.Parameters[(byte)ParameterCode.RotX];
		float rotY = (float)eventData.Parameters[(byte)ParameterCode.RotY];
		float rotZ = (float)eventData.Parameters[(byte)ParameterCode.RotZ];
		float rotW = (float)eventData.Parameters[(byte)ParameterCode.RotW];

		var client = Players.FirstOrDefault(n => n.CharacterName.Equals(characterName));
		if (client == null) {
			Debug.Log("Rot:not client");
			return;
		}

		client.newRotation = new Quaternion(rotX, rotY, rotZ, rotW);
	}

	private void JumpHandler(EventData eventData) {
		string characterName = (string)eventData.Parameters[(byte)ParameterCode.CharacterName];
		bool jump = (bool)eventData.Parameters[(byte)ParameterCode.Jump];

		var client = Players.FirstOrDefault(n => n.CharacterName.Equals(characterName));

		if (client == null) {
			Debug.Log("Jump:not client");
			return;
		}


		client.Jump = jump;
	}

	private void AnimHandler(EventData eventData) {
		string characterName = (string)eventData.Parameters[(byte)ParameterCode.CharacterName];
		float h = (float)eventData.Parameters[(byte)ParameterCode.H];
		float AnimPosY = (float)eventData.Parameters[(byte)ParameterCode.AnimPosY];
		float v = (float)eventData.Parameters[(byte)ParameterCode.V];
		bool graunded = (bool)eventData.Parameters[(byte)ParameterCode.Graunded];

		var client = Players.FirstOrDefault(n => n.CharacterName.Equals(characterName));

		if (client == null) {
			Debug.Log("Anim:not client");
			return;
		}
		
		client.AnimPosY = AnimPosY;
		client.H = h;
		client.V = v;
		client.Graunded = graunded;
	}

	private void FireHandler(EventData eventData) {
		string characterName = (string)eventData.Parameters[(byte)ParameterCode.CharacterName];

		Vector3 target = new Vector3();
		target.x = (float)eventData.Parameters[(byte)ParameterCode.TargetX];
		target.y = (float)eventData.Parameters[(byte)ParameterCode.TargetY];
		target.z = (float)eventData.Parameters[(byte)ParameterCode.TargetZ];
		Vector3 origin = new Vector3();
		origin.x = (float)eventData.Parameters[(byte)ParameterCode.OriginX];
		origin.y = (float)eventData.Parameters[(byte)ParameterCode.OriginY];
		origin.z = (float)eventData.Parameters[(byte)ParameterCode.OriginZ];
		string nameSkill = (string)eventData.Parameters[(byte)ParameterCode.NameSkill];

		var client = Players.FirstOrDefault(n => n.CharacterName.Equals(characterName));

		if (client == null) {
			Debug.Log("Fire:not client");
			return;
		}
		client.Target = target;
		client.Origin = origin;
		client.CurItem = items.FirstOrDefault(n => n.Value.nameSlill.Equals(nameSkill)).Value;
		client.Fire = true;
	}

	private void HitHandler(EventData eventData) {
		string characterName = (string)eventData.Parameters[(byte)ParameterCode.CharacterName];
		string targetName = (string)eventData.Parameters[(byte)ParameterCode.TargetName];
		float damage = (float)eventData.Parameters[(byte)ParameterCode.Damage];

		var client = Players.FirstOrDefault(n => n.CharacterName.Equals(targetName));

		if (client == null) {
			Debug.Log("Hit:not client");
			return;
		}
		client.HealthPoint -= damage;
	}

	private void WorldEnterHandler(EventData eventData) {

		string characterName = (string)eventData.Parameters[(byte)ParameterCode.CharacterName];
		float posX = (float)eventData.Parameters[(byte)ParameterCode.PosX];
		float posY = (float)eventData.Parameters[(byte)ParameterCode.PosY];
		float posZ = (float)eventData.Parameters[(byte)ParameterCode.PosZ];

		var obj = Instantiate(PlayerPrefab);
		obj.transform.position = new Vector3(posX, posY, posZ);

		var player = obj.AddComponent<Player>();
		player.CharacterName = characterName;
		player.SetStatus (GlobalStaticInfo.PlStat.HP, 1, GlobalStaticInfo.PlStat.MP, 1);
		player.Control = true;
		player.sendRate = 0.02f;
		if (characterName == CharacterName) {
			Controlller c = obj.GetComponent<Controlller>();
			var cam = Instantiate(CameraPrefab);
			player.items = new List<Item> ();
			for(int i = 0; i < items.Count; i++) {
				if (items.ContainsKey(i)) {
					player.items.Add(items[i]);
				}
				else player.items.Add(new Item());
			}
			cam.GetComponent<UnityStandardAssets.Cameras.FreeLookCam>().m_Target = obj.GetComponent<Controlller>().CamTarget.transform;
			player.isMine = true;
			player.mCam = cam.transform;
			player.SkillsBar ();
			c.enabled = true;
			c.OnEnabled (cam.transform, player);
		}

		Players.Add(player);

		Debug.Log("WorldEnterHandler charName:" + characterName);
	}

	private void WorldExitHandler(EventData eventData) {
		string characterName = (string)eventData.Parameters[(byte)ParameterCode.CharacterName];

		var client = Players.FirstOrDefault(n => n.CharacterName.Equals(characterName));
		Players.Remove(client);
		DestroyObject(client.gameObject);
		if(characterName == CharacterName)
			SceneManager.LoadScene("Menu");
		Debug.Log("WorldExitHandler charName:" + client.CharacterName);
	}

	private void ListPlayersHandler(OperationResponse operationResponse) {
		var dicPlayer = operationResponse.Parameters[(byte)ParameterCode.ListPlayers] as Dictionary<string, object[]>;
		if (dicPlayer == null) {
			Debug.Log("ListPlayers null!");
			return;
		}

		foreach (var p in dicPlayer) {
			string charName = p.Key;
			object[] pos = p.Value;
			if (charName != CharacterName) {
				var obj = Instantiate(PlayerPrefab);
				obj.transform.position = new Vector3((float)pos[0], (float)pos[1], (float)pos[2]);
				var player = obj.AddComponent<Player>();
				player.CharacterName = charName;
				player.sendRate = 0.02f;
				Players.Add(player);

				Debug.Log("Create player from list charName:" + charName);
			}
		}

		Debug.Log("ListPlayersHandler ");
	}

	private void ChoiceHostHandler(OperationResponse operationResponse) {
		int host = (int)operationResponse.Parameters[(byte)ParameterCode.NumHost];
		string nameScene = GlobalStaticInfo.Hosts.FirstOrDefault(n => n.numHost.Equals(host)).sceneeHost;
		Debug.Log(CharacterName + " in to " + nameScene);
		SceneManager.LoadScene(nameScene);
	}

	private void CreateHostHandler(EventData eventData) {
		if(CharacterName == (string)eventData.Parameters[(byte)ParameterCode.CharacterName]) {
			return;
		}
		int numHost = (int)eventData.Parameters[(byte)ParameterCode.NumHost];
		object [] hostInfo = (object [])eventData.Parameters[(byte)ParameterCode.HostInfo];
		Host host = new Host();
		host.numHost = (int)hostInfo[0];
		host.nameHost = (string)hostInfo[1];
		host.sceneeHost = (string)hostInfo[2];

		GlobalStaticInfo.Hosts.Add(host);

		Debug.Log("Create host: " + (string)hostInfo[1]);
	}

	#endregion

	#region Other Function

	public void AddSkills(int num, string nameSkill) {
		bool add = true;
		foreach(var i in items.Keys) {
			if (i == num) add = false;
		}
		if (add) items.Add(num, RegistryItems.FirstOrDefault(n => n.nameSlill.Equals(nameSkill)));
		else items[num] = RegistryItems.FirstOrDefault(n => n.nameSlill.Equals(nameSkill));
	}

	#endregion
}
