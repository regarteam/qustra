﻿using System;
using System.Collections;
using System.Collections.Generic;
using TestPhotonLib.Common;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour {
	string NamePlayer;
	PhotonServer photonServer;
	public string Error { get; set; }

	// Use this for initialization
	void Start () {
		photonServer = PhotonServer.Instance;
		NamePlayer = "";
		photonServer.OnLoginResponse += Game;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void Game(object sender, LoginEventArgs e) {
		if (e.Error != ErrorCode.Ok) {
			Error = "Error:" + e.Error.ToString();
			return;
		}

		PhotonServer.Instance.OnLoginResponse -= Game;
		PhotonServer.Instance.CharacterName = NamePlayer;
		SceneManager.LoadScene("Menu");
	}

	void OnGUI() {
		NamePlayer = GUI.TextField(new Rect(5, 5, 200, 20), NamePlayer);
		if (GUI.Button(new Rect(5, 40, 100, 30), "Вход") || Input.GetKeyDown(KeyCode.KeypadEnter)) {
			PhotonServer.Instance.SendLoginOperations(NamePlayer);
		}
	}
	
}
