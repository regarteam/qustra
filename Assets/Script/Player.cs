﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
	public bool Control { get; set; }
	protected Transform transform { get; set; }

	public bool isMine { get; set; }

	public string CharacterName { get; set; }

	public Vector3 Position {
		get { return transform.position; }
		set { transform.position = value; }
	}
	private Vector3 pos;
	public Vector3 OldPosition { get; set; }
	public float deltaTime { get; set; }
	public float syncTime { get; set; }

	public Vector3 newPosition { set; get; }
	public float k = 1;

	public Quaternion Rotation {
		get { return transform.rotation; }
		set { transform.rotation = value; }
	}

	public Quaternion OldRotation { get; set; }
	public Quaternion newRotation { set; get; }


	public bool Jump { get; set; }

	public Item CurItem { get; set; }
	public bool Fire { get; set; }
	public Vector3 Origin { get; set; }
	public Vector3 Target { get; set; }


	public float HealthPoint;
	public float ManaPoint { get; set; }
	public float RegenHealth { get; set; }
	public float RegenMana { get; set; }
	public bool Life { get; set; }
	public int Kills;
	public int Death;

	public bool Graunded { get; set; }
	public float V { get; set; }
	public float H { get; set; }
	public float AnimPosY { get; set; }
	public float sendRate;
	public Transform mCam { get; set; }
	public float lerp = 0.2f;
	public Vector3 Point;
	public List<Item> items{ get; set; }
	private Dictionary<string, Image> bar = new Dictionary<string, Image> ();
	private Image Hp;
	private Image Mp;
	public float reloadLife = 0;


	public float neutralMagicForce { get; set; }
	public Dictionary<Item.TypeForce, float> magicForce { get; set; }

	void Awake() {
		Life = true;
		transform = base.transform;
		newPosition = Position;
		Image b;
		for (int i = 0; i <= 6; i++) {
			b = GameObject.Find (i.ToString ()).GetComponent<Image>();
			bar.Add ("Skill-" + i.ToString (), b);
			b.gameObject.name = "Skill-" + i.ToString ();
		}
		Hp = GameObject.Find ("HP").GetComponent<Image>();
		Mp = GameObject.Find ("MP").GetComponent<Image>();
		magicForce = new Dictionary<Item.TypeForce, float>();

		neutralMagicForce = GlobalStaticInfo.PlStat.MagicForce;
		magicForce.Add(Item.TypeForce.Fire, GlobalStaticInfo.PlStat.FireForce);
		magicForce.Add(Item.TypeForce.Ice, GlobalStaticInfo.PlStat.IceForce);
		magicForce.Add(Item.TypeForce.Electro, GlobalStaticInfo.PlStat.ElectroForce);
		magicForce.Add(Item.TypeForce.Necro, GlobalStaticInfo.PlStat.NecroForce);
		if(GameObject.Find("Point"))
			Point = GameObject.Find("Point").transform.position;
		else {
			Point = Vector3.zero;
		}
	}

	private void FixedUpdate() {
		if(Life && HealthPoint <= 0) {
			Dead();
		}
		if (Life) {
			Regen();
			Move();
		}
		Bar ();
	}

	private void Bar(){
		float dh = HealthPoint / GlobalStaticInfo.MaxHp;
		float dm = ManaPoint / GlobalStaticInfo.MaxMp;
		if (dh > Hp.fillAmount) {
			Hp.fillAmount += 0.01f;
		}
		if (dh < Hp.fillAmount) {
			Hp.fillAmount -= 0.01f;
		}
		if (dm > Mp.fillAmount) {
			Mp.fillAmount += 0.01f;
		}
		if (dm < Mp.fillAmount) {
			Mp.fillAmount -= 0.01f;
		}
	}

	private void Regen(){
		if(HealthPoint < GlobalStaticInfo.MaxHp)
			HealthPoint += RegenHealth * Time.fixedDeltaTime;
		if(ManaPoint < GlobalStaticInfo.MaxMp)
			ManaPoint += RegenMana * Time.fixedDeltaTime;
	}

	public void SkillsBar(){
		if (items == null)
			return;
		if (items.Count != 0) {
			for (int i = 0; i<items.Count; i++) {
				bar ["Skill-" + i.ToString()].sprite = items [i].image;
			}
		}
	}

	public void SetStatus(float hp, float rh, float mp, float rm){
		HealthPoint = hp;
		GlobalStaticInfo.MaxHp = hp;
		ManaPoint = mp;
		GlobalStaticInfo.MaxMp = mp;
		RegenHealth = rh;
		RegenMana = rm;
	}

	void Move() {
		Vector3 direct;
		float _x;
		float _y;
		float _z;
		direct = newPosition - OldPosition;
		_x = Mathf.Lerp (OldPosition.x, newPosition.x, sendRate/2);
		_y = Mathf.Lerp (OldPosition.y, newPosition.y, sendRate/2);
		_z = Mathf.Lerp (OldPosition.z, newPosition.z, sendRate/2);
		Vector3 move = new Vector3 (_x, _y, _z);
		move = move - transform.position;
		pos = transform.position;
		transform.position += move;
		syncTime += Time.fixedDeltaTime;
		if (Position == newPosition)
			return;
		if (syncTime >= sendRate) {
			newPosition += direct.normalized;
			OldPosition = transform.position;
		}
		if(isMine)
		MoveCam (transform.position);
	}
	void MoveCam(Vector3 newPos){
		mCam.position = Vector3.Lerp (mCam.transform.position, newPos, lerp);
	}

	void Dead() {
		Control = false;
		Death += 1;
		Life = false;
		GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
	}
	void Spawn() {
		HealthPoint = GlobalStaticInfo.MaxHp;
		transform.position = Point;
		Control = true;
		Life = true;
		GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
		reloadLife = 0;
	}
	private void OnGUI() {
		if (Life)
			return;
		else {
			reloadLife += Time.deltaTime;
			GUI.Label(new Rect(new Vector2((Screen.width / 2 - Screen.width / 12), (Screen.height / 2 - Screen.height / 12)), new Vector2(200, 200)), Mathf.Floor(reloadLife).ToString());
			if (reloadLife > 10) {
				Spawn();
			}
		}
	}
}
