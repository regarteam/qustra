﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class NumSkills : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public void OnPointerEnter(PointerEventData eventData) {
		GlobalStaticInfo.Skill = true;
		GlobalStaticInfo.SkillBox = gameObject;
	}

	public void OnPointerExit(PointerEventData eventData) {
		GlobalStaticInfo.Skill = false;
	}
}
