﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DropSkills : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
	protected Transform transform { get; set; }

	public string NameSkill;
	public bool pointerStay;
	public bool dropThis;
	public bool drobOther;
	public Transform newCanvas;
	public Transform _parent;
	private Vector3 pos;
	public bool skill;
	public GameObject SkillBox;
	private void Start() {
		transform = base.transform;
		newCanvas = GameObject.Find("NewCanvas").transform;
		_parent = gameObject.transform.parent;
		pos = transform.position;
	}
	void Update() {
		skill = GlobalStaticInfo.Skill;
		SkillBox = GlobalStaticInfo.SkillBox;
		if (Input.GetMouseButtonUp(0) && skill && dropThis) {
			PhotonServer.Instance.AddSkills(Convert.ToInt32(SkillBox.name), NameSkill);
			SkillBox.GetComponent<Image>().sprite = gameObject.GetComponent<Image>().sprite;
		}
		if (!pointerStay && !dropThis) drobOther = Input.GetMouseButton(0);
		if (!drobOther) dropThis = Input.GetMouseButton(0);
		if (Input.GetMouseButton(0)) {
			if ((pointerStay || dropThis) && !drobOther) {
				if (transform.parent != newCanvas) {
					transform.SetParent(newCanvas);
					GetComponent<Image>().raycastTarget = false;
				}
				transform.position = Input.mousePosition;
			}
		}
		else {
			if (transform.parent != _parent) {
				transform.position = pos;
				transform.SetParent(_parent);
				GetComponent<Image>().raycastTarget = true;
			}
		}
	}

	public void OnPointerEnter(PointerEventData eventData) {
		pointerStay = true;
	}			

	public void OnPointerExit(PointerEventData eventData) {
		pointerStay = false;		
	}	


}	

