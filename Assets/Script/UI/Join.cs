﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Join : MonoBehaviour, IPointerClickHandler {
	public HostsPanel hostPanel;

	void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
		hostPanel.Join(transform.parent.gameObject);
	}
}
