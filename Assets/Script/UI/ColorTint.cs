﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

	public class ColorTint : MonoBehaviour {
		private float colorMax;
		private float step;
		private float cM = 0;
		private bool colorTint;
		private bool colorFlash;
		private bool begin = true;
		public int flash = 3;
		// Use this for initialization
		void Start() {

		}

		// Update is called once per frame
		void Update() {
			if (colorTint) {
				if (cM < colorMax) {
					cM = _ColorTint(cM, begin, step);
				}
				else {
					begin = !begin;
					cM -= step;
				}
				if (cM < 0) {
					colorTint = false;
					begin = true;
					cM = 1;
					_ColorTint(cM, begin, step);
				}
			}
			if (colorFlash) {
				if (cM < colorMax) {
					cM = _ColorFlash(cM, begin, step);
				}
				else {
					begin = !begin;
					cM -= step;
				}
				if (cM < 0) {
					flash -= 1;
					begin = true;
					cM = 1;
				}
				if(flash <= 0) {
					colorFlash = false;
					begin = true;
					cM = 1;
					_ColorTint(cM, begin, step);
					flash = 3;
				}
			}
		}

		private float _ColorTint(float m, bool _begin, float _step) {
			float col = (255 - m) / 255;
			Color c = new Color(col, col, col, 1);
			gameObject.GetComponent<Image>().color = c;
			if (_begin)
				return m + _step;
			else return m - _step;
		}

		private float _ColorFlash(float m, bool _begin, float _step) {
			float col = (127 - m) / 255;
			Color c = new Color(1, col, col, 1);
			gameObject.GetComponent<Image>().color = c;
			if (_begin)
				return m + _step;
			else return m - _step;
		}

		public void NewColorTint(float m, float s) {
			colorTint = true;
			colorMax = m;
			step = s;
		}

		public void NewColorFlash(float m, float s) {
			colorFlash = true;
			colorMax = m;
			step = s;
		}
	}
