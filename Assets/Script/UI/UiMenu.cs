﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

	public class UiMenu : MonoBehaviour {
	public HostsPanel hostPanel;
		public GameObject Field;
		public GameObject MainMenu;
		public GameObject PlayMenu;
		public GameObject Character;
		public GameObject[] SkillsPanels;
		public GameObject HostsField;
		public GameObject CreateHost;
		public Text NameHost;
		public Text SceneeHost;
		private string _nameHost;
		private string _sceneeHost;
		private bool isCreateHost;
		private float x = Screen.width/2;
		private float y = Screen.height/4;
		private int k = 0;
		public List<Button> ButtonsHost = new List<Button>();

		// Use this for initialization
		void Awake() {

		}

		// Update is called once per frame
		void Update() {
			if (isCreateHost) {
				_nameHost = NameHost.text;
			}
			Character.SetActive(PlayMenu.activeSelf);
		}

		void OnToMainMenu() {
			MainMenu.SetActive(true);
			PlayMenu.SetActive(false);
		}

		void OnToPlayMenu() {
			MainMenu.SetActive(false);
			PlayMenu.SetActive(true);
		}
		void OnToPlayMenuFromHosts() {
			HostsField.SetActive(false);
			PlayMenu.SetActive(true);
		}

		void Fire() {
			foreach (var i in SkillsPanels) {
				if (i.name == "Fire") i.SetActive(true);
				else i.SetActive(false);
			}
		}

		void Ice() {
			foreach (var i in SkillsPanels) {
				if (i.name == "Ice") i.SetActive(true);
				else i.SetActive(false);
			}
		}

		void Electric() {
			foreach (var i in SkillsPanels) {
				if (i.name == "Electric") i.SetActive(true);
				else i.SetActive(false);
			}
		}

		void Died() {
			foreach (var i in SkillsPanels) {
				if (i.name == "Died") i.SetActive(true);
				else i.SetActive(false);
			}
		}
		void ChiceHost() {
			PlayMenu.SetActive(false);
			HostsField.SetActive(true);
		}

		void OnCreateHost() {
			if (!isCreateHost) {
				CreateHost.SetActive(true);
				isCreateHost = true;
			}
		}
		void OffCreateHost() {
			CreateHost.SetActive(false);
			isCreateHost = false;
			_nameHost = "";
			_sceneeHost = "";
		}
		public void NameSceneHost(string name) {
			_sceneeHost = name;
		}

		void CreateHostAndPlay() {
			Host host = new Host();
			host.numHost = GlobalStaticInfo.Hosts.Count+1;
			host.nameHost = _nameHost;
			host.sceneeHost = _sceneeHost;
			host.maxClients = 10;
			GlobalStaticInfo.Hosts.Add(host);
			CreateHost.SetActive(false);
			isCreateHost = false;
			PhotonServer.Instance.CreateHostOperation(host.numHost, host.nameHost, host.sceneeHost);
		hostPanel.UpdateItems();
		}

		

		private void OnGUI() {
			
		}
	}
