﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HostsPanel : MonoBehaviour {
	public GameObject itemPrefab;
	public Dictionary<GameObject, Host> items;

	private void Awake() {
		items = new Dictionary<GameObject, Host>();

	}
	private void Start() {
		foreach (var i in GlobalStaticInfo.Hosts) {
			GameObject go = Instantiate(itemPrefab, transform);
			go.GetComponentInChildren<Text>().text = i.nameHost + " : " + i.sceneeHost;
			items.Add(go, i);
			go.GetComponentInChildren<Button>().gameObject.GetComponent<Join>().hostPanel = this;
		}
	}

	public void UpdateItems() {
		foreach(var i in items.Keys) {
			Destroy(i);
		}
		items.Clear();
		foreach(var i in GlobalStaticInfo.Hosts) {
			GameObject go = Instantiate(itemPrefab, transform);
			go.GetComponentInChildren<Text>().text = i.nameHost + " : " + i.sceneeHost;
			items.Add(go, i);
			go.GetComponentInChildren<Button>().gameObject.GetComponent<Join>().hostPanel = this;
		}
	}

	public void Join(GameObject go) {
		GlobalStaticInfo.NumHost = items[go].numHost;
		PhotonServer.Instance.ChoiceHostOperation(items[go].numHost);
	}
}
